//
//  Services.swift
//  Movies
//
//  Created by Fernando Cani on 17/09/18.
//  Copyright © 2018 Fernando Cani. All rights reserved.
//

import UIKit
import Alamofire

let api_key = "api_key=38c6416cf89cf210a86298cbdc9a4045"
let base_url = "https://api.themoviedb.org/3/"

class Services: NSObject {

    static func getAll(callback: @escaping (ResponseNowPlaying?, String?) -> Void) {
        let url = base_url + "movie/now_playing?" + api_key + "&language=pt-BR&region=BR"
        Alamofire.request(url).responseString { response in
            if let json = response.result.value {
                do {
                    //print(json)
                    let response = try JSONDecoder().decode(ResponseNowPlaying.self, from: json.toData())
                    callback(response, nil)
                } catch {
                    print(error)
                    callback(nil, "\(error)")
                }
            } else {
                callback(nil, "")
            }
        }
    }
    
    static func search(string: String, page: Int, callback: @escaping (ResponseSearch?, String?) -> Void) {
        let url = base_url + "search/movie?" + api_key + "&language=pt-BR&region=BR&query=" + string.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)! + "&page=\(page)"
        Alamofire.request(url).responseString { response in
            if let json = response.result.value {
                do {
                    //print(json)
                    let response = try JSONDecoder().decode(ResponseSearch.self, from: json.toData())
                    callback(response, nil)
                } catch {
                    print(error)
                    callback(nil, "\(error)")
                }
            } else {
                callback(nil, "")
            }
        }
    }
    
}
