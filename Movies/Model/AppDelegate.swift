//
//  AppDelegate.swift
//  Movies
//
//  Created by Fernando Cani on 17/09/18.
//  Copyright © 2018 Fernando Cani. All rights reserved.
//

import UIKit
import CoreData

let defaults = UserDefaults.standard

let PREFS_NOW_PLAYING = "PREFS_NOW_PLAYING"

var prefsNowPlaying: [Movie]? {
    get {
        if let data = defaults.object(forKey: PREFS_NOW_PLAYING) as? Data, let loadedObject = try? JSONDecoder().decode([Movie].self, from: data) {
            return loadedObject
        }
        return nil
    }
    set (movies) {
        if let encoded = try? JSONEncoder().encode(movies) {
            defaults.set(encoded, forKey: PREFS_NOW_PLAYING)
        }
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = self.createMainView()
        window.makeKeyAndVisible()
        self.window = window
        return true
    }
    
    func createMainView() -> UINavigationController {
        let home = HomeViewController(nibName: "HomeViewController", bundle: nil)
        return UINavigationController(rootViewController: home)
    }
    
    func applicationWillResignActive(_ application: UIApplication) { }
    
    func applicationDidEnterBackground(_ application: UIApplication) { }
    
    func applicationWillEnterForeground(_ application: UIApplication) { }
    
    func applicationDidBecomeActive(_ application: UIApplication) { }
    
    func applicationWillTerminate(_ application: UIApplication) { }
    
    static func getInstance() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
}

