//
//  Movies.swift
//  Movies
//
//  Created by Fernando Cani on 17/09/18.
//  Copyright © 2018 Fernando Cani. All rights reserved.
//

import UIKit

struct ResponseNowPlaying: Codable {
    var results: [Movie]?
}

struct ResponseSearch: Codable {
    var page: Int?
    var total_results: Int?
    var total_pages: Int?
    var results: [Movie]?
}

struct Movie: Codable {
    
    var vote_count: Int?
    var id: Int?
    var video: Bool?
    var vote_average: Float?
    var title: String?
    var popularity: Float?
    var poster_path: String?
    var original_language: String?
    var original_title: String?
    //    var genre_ids": [
    //    27,
    //    878,
    //    28,
    //    35
    //    ],
    var backdrop_path: String?
    var adult: Bool?
    var overview: String?
    var release_date: String? //2018-09-13
    
}
