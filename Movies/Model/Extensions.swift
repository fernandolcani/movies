//
//  Extensions.swift
//  Movies
//
//  Created by Fernando Cani on 17/09/18.
//  Copyright © 2018 Fernando Cani. All rights reserved.
//

import UIKit
import Alamofire

extension Data {
    
    func toString(encoding: String.Encoding = String.Encoding.utf8) -> String {
        let string = String(data: self, encoding: encoding)
        return string ?? ""
    }
}

extension String {
    
    func toData(encoding: String.Encoding = String.Encoding.utf8) -> Data {
        let data = self.data(using: encoding)
        return data ?? Data()
    }
    
    ///dateFormat = yyyy-MM-dd, dateFormatReturn = "dd/MM/yyyy", locale = "pt_BR"
    func getDate(dateFormat: String = "yyyy-MM-dd", dateFormatReturn: String = "dd/MM/yyyy", locale: String = "pt_BR") -> String {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: locale)
        formatter.dateFormat = dateFormat
        guard let date = formatter.date(from: self) else { return self }
        formatter.dateFormat = dateFormatReturn
        return formatter.string(from: date)
    }
    
}

extension String: ParameterEncoding {
    
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try urlRequest.asURLRequest()
        request.httpBody = data(using: .utf8, allowLossyConversion: false)
        return request
    }
    
}
