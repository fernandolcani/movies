//
//  HomeViewController.swift
//  Movies
//
//  Created by Fernando Cani on 17/09/18.
//  Copyright © 2018 Fernando Cani. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    lazy var controller = HomeController()
    var movies: [Movie] = [Movie]() {
        didSet {
            prefsNowPlaying = self.movies
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Top 10"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(self.btnSearch(_:)))
        self.tableView.register(UINib(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeTableViewCell")
        self.tableView.tableFooterView = UIView()
        if let movies = prefsNowPlaying {
            self.movies = movies
        }
        self.controller.refresh(callbackSuccess: { (movies) in
            self.movies = movies
            //self.navigationController?.pushViewController(MovieDetailViewController(movie: self.movies[0]), animated: true)
        }) { (error) in
            print(error)
        }
    }
    
    @objc func btnSearch(_ sender: UIBarButtonItem) {
        self.navigationController?.pushViewController(SearchViewController(), animated: true)
    }

}

extension HomeViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 164
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as! HomeTableViewCell
        cell.setup(position: indexPath.row + 1, movie: self.movies[indexPath.row])
        return cell
    }
    
}

extension HomeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigationController?.pushViewController(MovieDetailViewController(movie: self.movies[indexPath.row]), animated: true)
    }
    
}
