//
//  HomeController.swift
//  Movies
//
//  Created by Fernando Cani on 17/09/18.
//  Copyright © 2018 Fernando Cani. All rights reserved.
//

import UIKit

class HomeController {

    func refresh(callbackSuccess: @escaping (([Movie]) -> Void), callbackError: @escaping ((String) -> Void)) {
        Services.getAll { (responseNowPlaying, error) in
            if let responseNowPlaying = responseNowPlaying {
                if let results = responseNowPlaying.results {
                    var newResults = [Movie]()
                    results.forEach({
                        if let vote_average = $0.vote_average, vote_average >= Float(5.0) {
                            newResults.append($0)
                        }
                    })
                    callbackSuccess(newResults.sorted(by: { $0.vote_average! > $1.vote_average! } ))
                } else {
                    callbackError("strLoginError")
                }
            } else if let error = error {
                callbackError(error)
            } else {
                callbackError("strUnknowError")
            }
        }
    }
    
}
