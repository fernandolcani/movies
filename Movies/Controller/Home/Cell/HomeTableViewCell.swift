//
//  HomeTableViewCell.swift
//  Movies
//
//  Created by Fernando Cani on 17/09/18.
//  Copyright © 2018 Fernando Cani. All rights reserved.
//

import UIKit
import AlamofireImage

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var imgCover:         UIImageView!
    @IBOutlet weak var lblTitle:         UILabel!
    @IBOutlet weak var lblOriginalTitle: UILabel!
    @IBOutlet weak var lblRating:        UILabel!
    @IBOutlet weak var lblOverview:      UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setup(position: Int? = nil, movie: Movie) {
        if let poster_path = movie.poster_path, let image_url = URL(string: "https://image.tmdb.org/t/p/w200" + poster_path) {
            self.imgCover.af_setImage(withURL: image_url)
        }
        if let title = movie.title {
            self.lblTitle.text = title
            if let position = position {
                self.lblTitle.text = String(position) + " - " + title
            }
        }
        if let original_title = movie.original_title {
            var string = "(" + original_title + ")"
            if let release_date = movie.release_date {
                string += (" - " + release_date.getDate(dateFormatReturn: "yyyy"))
            }
            self.lblOriginalTitle.text = string
        }
        if let rating = movie.vote_average {
            self.lblRating.text = String(rating)
        }
        if let overview = movie.overview {
            self.lblOverview.text = overview
        }
    }
    
}
