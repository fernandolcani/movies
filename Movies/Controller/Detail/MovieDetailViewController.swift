//
//  MovieDetailViewController.swift
//  Movies
//
//  Created by Fernando Cani on 17/09/18.
//  Copyright © 2018 Fernando Cani. All rights reserved.
//

import UIKit
import AlamofireImage

class MovieDetailViewController: UIViewController {

    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var imgPicture: UIImageView!
    @IBOutlet weak var lblReleasedDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblOriginalTitle: UILabel!
    @IBOutlet weak var lblOverview: UILabel!
    
    var movie: Movie
    
    required init(movie: Movie) {
        self.movie = movie
        super.init(nibName: "MovieDetailViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Detalhe"
        if let cover = self.movie.backdrop_path, let url = URL(string: "https://image.tmdb.org/t/p/w400" + cover) {
            self.imgCover.af_setImage(withURL: url)
        }
        if let poster_path = self.movie.poster_path, let image_url = URL(string: "https://image.tmdb.org/t/p/w200" + poster_path) {
            self.imgPicture.af_setImage(withURL: image_url)
        }
        if let release_date = self.movie.release_date {
            self.lblReleasedDate.text = release_date.getDate(dateFormatReturn: "yyyy")
        }
        if let title = self.movie.title {
            self.lblTitle.text = title
        }
        if let original_title = self.movie.original_title {
            self.lblOriginalTitle.text = "(" + original_title + ")"
        }
        if let overview = self.movie.overview {
            self.lblOverview.text = overview
        }
    }

}
