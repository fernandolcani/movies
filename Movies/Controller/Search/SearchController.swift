//
//  SearchController.swift
//  Movies
//
//  Created by Fernando Cani on 18/09/18.
//  Copyright © 2018 Fernando Cani. All rights reserved.
//

import UIKit

class SearchController {

    func search(string: String, page: Int = 1, callbackSuccess: @escaping ((ResponseSearchObject) -> Void), callbackError: @escaping ((String) -> Void)) {
        print(string + String(page))
        Services.search(string: string, page: page) { (responseSearch, error) in
            if let responseSearch = responseSearch {
                if let page = responseSearch.page, let total_results = responseSearch.total_results, let total_pages = responseSearch.total_pages, let results = responseSearch.results {
                    callbackSuccess(ResponseSearchObject(page: page, total_results: total_results, total_pages: total_pages, movies: results))
                } else {
                    callbackError("strLoginError")
                }
            } else if let error = error {
                callbackError(error)
            } else {
                callbackError("strUnknowError")
            }
        }
    }
    
}

struct ResponseSearchObject {
    var page: Int = 0
    var total_results: Int = 0
    var total_pages: Int = 0
    var movies: [Movie] = [Movie]()
}
