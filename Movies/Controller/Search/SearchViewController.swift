//
//  SearchViewController.swift
//  Movies
//
//  Created by Fernando Cani on 18/09/18.
//  Copyright © 2018 Fernando Cani. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    lazy var controller = SearchController()
    var searchString = ""
    var responseObject: ResponseSearchObject = ResponseSearchObject()
    var movies: [Movie] = [Movie]() {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    required init() {
        super.init(nibName: "SearchViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Buscar"
        self.tableView.register(UINib(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeTableViewCell")
        self.tableView.tableFooterView = UIView()
        self.searchBar.becomeFirstResponder()
    }
    
    func clear() {
        self.title = "Search"
        self.movies.removeAll()
        self.tableView.reloadData()
        self.searchString = ""
    }

}

extension SearchViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 164
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == self.movies.count - 5 {
            if self.responseObject.page < self.responseObject.total_pages {
                self.controller.search(string: self.searchString, page: self.responseObject.page + 1, callbackSuccess: { (response) in
                    self.responseObject = response
                    self.movies.append(contentsOf: response.movies)
                }) { (error) in
                    print(error)
                }
            }
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as! HomeTableViewCell
        cell.setup(movie: self.movies[indexPath.row])
        return cell
    }
    
}

extension SearchViewController: UITableViewDelegate {
 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigationController?.pushViewController(MovieDetailViewController(movie: self.movies[indexPath.row]), animated: true)
    }
    
}

extension SearchViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let searchBartext = searchBar.text, searchBartext.isEmpty {
            self.clear()
            return
        }
        self.searchString = searchText
        self.controller.search(string: searchText, callbackSuccess: { (response) in
            self.movies = response.movies
            self.responseObject = response
            self.title = String(response.total_results) + (response.total_results == 1 ? " item" : " itens")
        }) { (error) in
            print(error)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
}
